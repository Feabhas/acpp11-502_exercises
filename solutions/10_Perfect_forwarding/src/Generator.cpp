// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Generator.h"
#include "Pipe.h"
#include <cassert>
#include <iostream>
#include <random>

using namespace std;

namespace {

std::default_random_engine gen{};

std::uniform_int_distribution random_alarm_type{
    static_cast<int>(Alarm::Type::advisory),
    static_cast<int>(Alarm::Type::warning)};

auto random_alarm() {
  return (static_cast<Alarm::Type>(random_alarm_type(gen)));
}

constexpr int max_list_size{10};
std::uniform_int_distribution num_of_alarms{1, max_list_size};

auto generate_random_alarm_list_size() {
  return num_of_alarms(gen);
}

constexpr std::array<const char*,6> alarm_strings{
  "Panic!",
  "Run away!",
  "Ignore this alarm.",
  "Oooops!",
  "Things are going horribly wrong.",
  "Please fix immediately."
};

std::uniform_int_distribution num_strings{
    0, static_cast<int>(alarm_strings.size() - 1)};

const char* random_string() {
  return (alarm_strings[static_cast<size_t>(num_strings(gen))]);
}
} // namespace

void Generator::execute() {
  assert(output);

  cout << "GENERATOR : ----------------------------------" << endl;

  auto alarm_ptr = make_unique<Alarm_list>();

  auto num_alarms = generate_random_alarm_list_size();
  cout << "Generating " << num_alarms << " alarm"
       << (num_alarms != 1 ? "s" : "") << endl;

  alarm_ptr->reserve(static_cast<size_t>(num_alarms));

  for (int i{0}; i < num_alarms; ++i) {
    alarm_ptr->emplace(random_alarm(), random_string());
  }

  output->push(move(alarm_ptr));

  cout << endl;
}

void connect(Generator& gen, Pipe& pipe) {
  gen.output = &pipe;
}
