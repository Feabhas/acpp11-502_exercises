// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------
#include "Alarm_filter.h"
#include "Display.h"
#include "Generator.h"
#include "Pipe.h"
#include "Pipeline.h"

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using namespace std;
using namespace chrono_literals;

int main(int argc, char** argv) {
  int const run_count = (argc > 1) ? std::stoi(argv[1]) : 1;

  Pipe pipe1{};
  Pipe pipe2{};

  Generator generator{};
  Display display{};
  Alarm_filter filter{Alarm::advisory};

  connect(generator, pipe1);
  connect(filter, pipe1, pipe2);
  connect(display, pipe2);

  // Flag to allow the Generator thread
  // to force the other threads to terminate
  bool done{};

  auto run_forever = [&done](Filter& filter) {
    while (!done) {
      filter.execute();
      this_thread::yield();
    }
  };

  std::thread gen_thread{[run_count, &done](Filter& filter) {
                           for (int i{}; i < run_count; ++i) {
                             filter.execute();
                             this_thread::sleep_for(500ms);
                           }
                           done = true;
                         },
                         ref(generator)};

  std::thread filter_thread{run_forever, ref(filter)};
  std::thread display_thread{run_forever, ref(display)};
  // Wait for the threads to
  // finish
  //
  gen_thread.join();
  filter_thread.join();
  display_thread.join();
}
